#!/usr/bin/env python
import socket, array, time, string,sys,random,threading,pickle,datetime

class ClientThread(threading.Thread):

    # Override Thread's __init__ method to accept the parameters needed:
    def __init__(self, channel, details):
        self.channel = channel
        self.details = details
        threading.Thread.__init__(self)
    
    def lock2key2(self,lock):
        "Generates response to $Lock challenge from Direct Connect Servers"
        lock = array.array('B', lock)
        ll = len(lock)
        key = list('0'*ll)
        for n in xrange(1,ll):
            key[n] = lock[n]^lock[n-1]
        key[0] = lock[0] ^ lock[-1] ^ lock[-2] ^ 5
        for n in xrange(ll):
            key[n] = ((key[n] << 4) | (key[n] >> 4)) & 255
        result = ""
        for c in key:
            if c in (0, 5, 36, 96, 124, 126):
                result += "/%%DCN%.3i%%/" % c
            else:
                result += chr(c)
        return result

    def run(self):
        flag=0
        print 'Received connection:', self.details[0]
        self.channel.settimeout(10)
        outfile=open("./xml_files/"+self.details[0]+'.xml','wb')
        print 'file opened to write'
	prev_buff="XXXX"
        while True:
            buff = ""
            try:
                while True:
                    t = self.channel.recv(1)
                    if t != '|':
			    buff += t
	            	    if(flag==1):
	   		    	outfile.write(t)
                    else:break
            except socket.timeout: pass
            except socket.error, msg: break
	    if(buff==prev_buff):
		    break
	    prev_buff=buff
	#   print buff
            lock=buff.split()
            if len(lock)>1 and lock[0] == '$MyNick':
  	        self.channel.send('$MyNick xanderb0t|') #this sent to channel
  	      	self.channel.send('$Lock EXTENDEDPROTOCOLABCABCABCABCABCABC Pk=DCPLUSPLUS0.75|') #this sent to channel
            if len(lock)>1 and lock[0] == '$Lock':
                self.channel.send('$Supports MiniSlots XmlBZList ADCGet TTHL TTHF GetZBlock ZLIG |')
                self.channel.send('$Direction Download 31604|')
                self.channel.send('$Key '+self.lock2key2(lock[1])+'|')
                self.channel.send('$ADCGET file files.xml 0 -1|')
            if len(lock)>1 and lock[0] == '$ADCSND':
		flag=1
       	self.channel.close()
        print 'Download complete'
        print 'Closed connection:', self.details[0]


class pyminidcbot2:
    HOST = '10.2.16.126'
    PORT = 411
    nick = 'xanderb0t'
    sharesize='45012000000'
    debugflag=1
    commanddebug=1
    ownernick='xander-cage'
    loggedon=0
    havenicklist=1
    myip='10.1.35.130'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tmpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    def saveXML(self,nicklist):
    	fn=open("nick_info","rb")
	fnr=fn.readlines()
	pre=[]
	for f in fnr:
		pre.append(f.split()[0])
	#print pre
    	nicks=self.getnicklist(nicklist)
	#print nicks
    	ports=[11975,11493,13036,13054,12461]
	l=len(nicks)
	for i in xrange(1,l):
		if(nicks[i] in pre):
			print "Already Downloaded !!"
			continue
    		prtnum=random.randint(0,4)
		port=ports[prtnum]
        	self.downloadfilelist(nicks[i],port)
		time.sleep(5)

    def readsock(self,sock):
        buff = ""
        sock.settimeout(0.13)
        while True:
            try:
                while True:
                    t = sock.recv(1)
                    if t != '|': buff += t
                    else: return buff
            except socket.timeout:
                pass    
            except socket.error, msg:
                return 
        
        return buff

    def readconn(self,conn):
        buff = ""
        conn.settimeout(0.13)
        while True:
            try:
                while True:
                    t = conn.recv(1)
                    if t != '|': buff += t
                    else: return buff
            except socket.timeout:
                pass  
            except socket.error, msg:
                return
        return buff

        return

    def getnicklist(self,nicks):
        nicklist=nicks.split("$$")
        print 'processing nicklist'
	#print nicks
        #print len(nicklist)
        self.havenicklist=1
        return nicklist

    def parsecommand(self,gotstring):
        if (gotstring != ''):
            str = gotstring.split()
            if (self.debugflag): print 'DEBUG: '+gotstring
            if str[0] == '$Lock':
                self.s.send('$Key '+self.lock2key2(str[1])+'|')
                self.s.send('$ValidateNick '+self.nick+'|')
                self.s.send('$GetNickList '+'|')
            elif str[0] == '$UserIP': # $Hello
                    if (self.debugflag): print '\nGot $UserIP'
            elif str[0] == '$Hello':
                    self.s.send('$Version 1,0091|')
                    if (self.debugflag): print 'DEBUG: Sending $Version'
                    self.s.send('$MyINFO $ALL '+self.nick+' MFcking python bot$ $100$mfckingbot@bot.com$'+self.sharesize+'$|')
        	    #self.downloadfilelist("jack",12345)
                    if (self.debugflag): print 'DEBUG: Sending $MyINFO'
            elif str[0] == '$NickList':
	    	    self.saveXML(str[1])
        	    self.downloadfilelist("jack",12345)
            elif str[0] == '$HubTopic':
                    if (self.debugflag): print 'DEBUG: Got $HubTopic - login complete'
                    return 'Logged'
            if str[0]: return str[0]
        return
    	
    def lock2key2(self,lock):
        "Generates response to $Lock challenge from Direct Connect Servers"
        lock = array.array('B', lock)
        ll = len(lock)
        key = list('0'*ll)
        for n in xrange(1,ll):
            key[n] = lock[n]^lock[n-1]
        key[0] = lock[0] ^ lock[-1] ^ lock[-2] ^ 5
        for n in xrange(ll):
            key[n] = ((key[n] << 4) | (key[n] >> 4)) & 255
        result = ""
        for c in key:
            if c in (0, 5, 36, 96, 124, 126):
                result += "/%%DCN%.3i%%/" % c
            else:
                result += chr(c)
        return result
    def loginloop(self):
        while 1:
             data=self.readsock(self.s)
             t=self.parsecommand(data)
             if t =='Logged': break
        return
    
    def logintohub(self):
        print 'connecting....'
        self.s.connect((self.HOST, self.PORT))
        self.s.send('Hello, world|')
        self.loginloop()
        return
    def rawprint(self,message):
        self.s.send(message)

    def downloadfilelist(self,nick,port):
        timeout=15
        starttime=time.time()
        #create an INET, STREAMing socket
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind(('', port))
        server.settimeout(timeout)
        #become a server socket
	#client ip
    	client_ip='10.2.56.71'
        self.rawprint('$ConnectToMe '+nick+' '+client_ip+':'+str(port)+'|') #establish a connect with client

        print 'listening socket....111'
        server.listen(5)
        while time.time() <= starttime+timeout:
            try:
                print 'going....'
                channel, details = server.accept()
		print nick,details[0]
		ts = time.time()
    		st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		f=open("nick_info","ab")
		f.write(nick+"\t\t\t"+details[0]+"\t\t"+st+"\n")
		f.close()
                ClientThread(channel, details).run()
            except socket.timeout:
                print 'timeout :('
                break
        return

t=pyminidcbot2()
t.logintohub()
