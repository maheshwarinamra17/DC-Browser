import os
from xml.dom import minidom

xmldir = "./xml_files/done/"
files = os.listdir(xmldir)
benchm_size = 500000000
tot = len(files)
lent = 0
f = open("PseudoMovies.txt","wb")
n=1
for fil in files:
	try:
		print "File Name",fil,"started"
		xmldoc = minidom.parse(xmldir+fil)
		itemlist = xmldoc.getElementsByTagName('File') 
	except:
		print "Error in File",fil
		continue
	lent = lent + len(itemlist)
	for s in itemlist :
		size = int(s.attributes['Size'].value)
		s = (s.attributes['Name'].value).encode('utf-8')+"\n"
		z = s.split(".")
		if(size >= benchm_size and (z[-1]=='avi' or z[-1]=='mkv' or z[-1]=='mp4')):
			f.write(s)
	print "File number" ,n, "completed out of",tot
	n = n+1
print lent
f.close()
